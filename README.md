# Personal website

Currently the site is hosted under [jacek.lysiak.ovh](https://jacek.lysiak.ovh).

Source code of my personal website and some helper scripts.

I use [Hugo](https://gohugo.io/) framework to generate a static webpage
from a bunch of Markdown files.

`content`, `data`, and `static` directories are included as external
dependencies to decouple work on the content, web framework and configuration.

## Cloning

Although it's a personal website, you can clone the skeleton and build your own
page. I'm releasing my setup under [GPL](./LICENSE). To clone the repo use:
> git clone https://gitlab.com/jlysiak-content/website.git --recursive

The content is shared under another license. See
[content](https://gitlab.com/jlysiak-content/content.git) repository.
If you use contents from this page, please, cite. Thank you!


## Debug server

> make debug-server

It calls `hugo server -D` which runs development server. `-D` tells the server
to render draft posts.
