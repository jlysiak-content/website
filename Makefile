public:
	hugo -v

deploy: public
	rsync -avz --delete public/ empress:~/website/

submodules-init:
	git submodule update --init --recursive

submodules-update:
	git submodule update --remote --merge

debug-server:
	hugo server -D

.PHONY: debug-server submodules-init submodules-update public

